import React from 'react'
import Container from '@material-ui/core/Container';
import Myself from './images/myself.jpg';
import Computer from './images/hero.svg';
import  "./main.css"
import {Animated} from "react-animated-css";

export default function Banner() {

    return (
        <div>
            <Animated animationIn="fadeInUp" animationInDuration="2000" animationOut="fadeOut" isVisible={true}>

        <div style={{padding: "48px 24px", paddingBottom: '0px'}} >
      <Container maxWidth="lg">
            <h1 style={{textAlign: 'center', fontWeight: '700', fontSize: '48px'}}>Full Stack Web Developer</h1>
            <p style={{textAlign: 'center', fontSize: '22px', color:'rgba(10, 10, 10, 0.9)'}}>I'am a Full Stack Web Developer <br /> driven to work with passion and determination.</p>

            <p style={{textAlign: 'center', padding: '2%'}}><img src={Myself} style={{height: '150px', borderRadius: '50%'}} className={'myself-image'} /></p>
            <p style={{textAlign: 'center', marginTop: '3%'}}><img src={Computer} className={'svg'} /></p>
            </Container>
        </div>
            <section className={'section'}>
                <div style={{padding: '5.8%'}}>
            <Container maxWidth="md">
                <h2 style={{textAlign: 'center', color: 'white', fontSize: '32px'}} className={'calvin'}>Hi, I’m Calvin. Nice to meet you.</h2>
                <p style={{textAlign: 'center', fontSize:' 20px', color: 'white'}}>I graduated as a top 1 performer out of 22 bootcampers
                 and received several awards. I worked as a full-stack web developer on e-commerce companies like flowerstore.ph, rebox.com.sg and Icelerate LLC
                 where as I enhanced my skills both back-end and frontend in web development,
                 Rest api, integrations, scripts, full-flow systems. I use different programming languages and frameworks like Php, Laravel, Node.js, React, MySQL.</p>
</Container>
                </div>
            </section>
            </Animated>
            </div>
    )
}