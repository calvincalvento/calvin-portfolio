import React from "react";
import './main.css'
import Container from '@material-ui/core/Container';
import Grid from "@material-ui/core/Grid";
import Card from '@material-ui/core/Card';
import  "./main.css"
import Gab from './images/testimonials.jfif'
import backend from './images/backend.svg'
import tools from './images/other-tools.svg'
import Ramil from './images/ramil.jpg'
import Glenn from './images/glenn.PNG'
import Projects from './projects'
import {AnimatedOnScroll} from "react-animated-css-onscroll";

export default function Testimonials() {
  return (

      <div>
      <div className={'testimonials'}>
          <Container maxWidth="md" >
      <h1 style={{color: 'white'}}>Testimonials</h1>
      <p style={{textAlign: 'center', fontSize:' 20px', color: 'white'}}>For over the years past working as a Full Stack Web Developer, Here are the some people that I used to work on my projects.</p>
    </Container>
    </div>
     <Container maxWidth="md" style={{marginTop : '-8%' }}>
       
     <div className={'testimonial-cards'}>


 <Grid container  spacing={4}>
   <Grid item md={4} xs={12}>
     <AnimatedOnScroll animationIn="fadeInUp" animationInDuration="2000"  isVisible={true}>
     <Card className={'testimonials-cards'}>
       <p style={{textAlign: 'center'}}><img src={Gab} style={{borderRadius: '50%', height: '100px'}} /></p>
       <h3 style={{textAlign: 'center', marginTop: '1em',  color : '#7510f7', fontWeight: 'bold'}}>Gabrielle Cordero</h3>
       <p style={{fontStyle: 'italic', textAlign: 'center', fontSize: '12px'}}> Full Stack Web Developer</p>
       <p style={{textAlign: 'center', fontSize: '13px'}}>“Calvin's a clear communicator with the tenacity and confidence to really dig in to tricky function scenarios and the collaborative friction that's needed to produce excellent work.”</p>
      
       

     </Card>
    </ AnimatedOnScroll>
   </Grid>

   <Grid item md={4} xs={12}>
    <AnimatedOnScroll animationIn="fadeInUp" animationInDuration="2000" animationInDelay='500' isVisible={true}>
   <Card className={'testimonials-cards'}>
       <p style={{textAlign: 'center'}}><img src={Ramil} style={{borderRadius: '50%',height: '100px'}} /></p>
       <h3 style={{textAlign: 'center', marginTop: '1em', fontWeight: 'bold',  color : '#7510f7', fontWeight: 'bold'}}>Ramil Saavedra</h3>
       <p style={{fontStyle: 'italic', textAlign: 'center', fontSize: '12px'}}> Front-end Web Developer</p>

       <p style={{textAlign: 'center', fontSize: '13px'}}>“Calvin's passion on programming made me easy to collaborate with him on projects that we created. He can easily adapt and implements what is best on our code structures.”</p>
      
       <br />

     </Card>
   </AnimatedOnScroll>
   </Grid>
   <Grid item md={4} xs={12}>
   <AnimatedOnScroll animationIn="fadeInUp" animationInDuration="2000" animationInDelay="1000"  isVisible={true}>
   <Card className={'testimonials-cards'}>

       <p style={{textAlign: 'center'}}><img src={Glenn} style={{borderRadius: '50%', height: '100px'}} /></p>
       <h3 style={{textAlign: 'center', marginTop: '1em',  color : '#7510f7', fontWeight: 'bold'}}>Glenn Losentes</h3>
       <p style={{fontStyle: 'italic', textAlign: 'center', fontSize: '12px'}}> Senior Web Developer</p>

       <p style={{textAlign: 'center', fontSize: '13px'}}>“Calvin was a real pleasure to work with and we look forward to working with him again. He’s definitely the kind of programmer you can trust with a project from start to finish.”</p>
      
     </Card>
     </AnimatedOnScroll>
   </Grid>
 </Grid>
 </div>
 {/* <Projects /> */}
 </Container>
 </div>
  );
}
