import React from "react";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import LaptopChromebookIcon from "@material-ui/icons/LaptopChromebook";
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import GitHubIcon from '@material-ui/icons/GitHub';
import InstagramIcon from '@material-ui/icons/Instagram';
import Resume from './calvin-calvento-resume.pdf'
export default function Footer() {

  function redirectWebsite(link){

    window.location.replace(link)
  }
  return (
    <div style={{ marginTop: "10%" }} id="footer">
      <Container maxWidth="lg">
        <div className={"sub-footer"}>
          <Grid container spacing={4}>
            <Grid item md={4} xs={12}>
              <h1 style={{ textAlign: "center", color: "white" }}>Hire me!</h1>
            </Grid>
            <Grid item md={4} xs={12}>
              <p style={{ textAlign: "center", color: "white" }}>
                Interested to hire me? Message me via messenger or download my
                resume for more information.
              </p>
            </Grid>
            <Grid item md={4} xs={12}>
              <h1 style={{ textAlign: "center" }}>
                {" "}
                <a href={Resume} download>
                <Button
                  variant="outlined"
                  color="primary"
                  style={{
                    color: "white",
                    borderRadius: "290486px",
                    border: "2px solid #7510F7",
                    padding: "10px",
                  }}
                  className={"resume-button"}
                >
                  DOWNLOAD RESUME
                </Button>
               </a>

              </h1>
            </Grid>
          </Grid>
        </div>
      </Container>

      <div className={"footer"}>
        <Container maxWidth="md">
          <p style={{ textAlign: "center", marginTop:' 25px'}}>
            <LaptopChromebookIcon
              style={{ color: "white", fontSize: "70px" }}
            />
          </p>
          <h3 style={{ textAlign: "center", color: "white" }}>
            A good programmer always looks both ways before crossing a one-way
            street
          </h3>
        </Container>

        <Container maxWidth="sm" style={{marginTop:'55px'}}
        >

      <Grid container spacing={2}  direction="row"
  justify="center"
  alignItems="center">
        <Grid item md={2} xs={4}>
        <p style={{textAlign: 'center'}}>

            <Button
                  variant="outlined"
                  color="primary"
                  style={{color:'white', border: '2px solid white', borderRadius: '290486px'}}
                  className={'footer-icon'}
                  onClick={() => redirectWebsite('https://linkedin.com/in/calvin-calvento-53b6a818b')}

                  >
                 <LinkedInIcon />
                </Button>
                  </p>
        </Grid>
        <Grid item md={2} xs={4}>
        <p style={{textAlign: 'center'}}>

        <Button
                  variant="outlined"
                  color="primary"
                  style={{color:'white', border: '2px solid white', borderRadius: '290486px'}}
                  className={'footer-icon'}
                  onClick={() => redirectWebsite('https://www.facebook.com/calvin.calvento')}

                    >
                 <FacebookIcon />
                </Button>
                  </p>
        </Grid>
        <Grid item md={2} xs={4}>
        <p style={{textAlign: 'center'}}>

        <Button
                  variant="outlined"
                  color="primary"
                  style={{color:'white', border: '2px solid white', borderRadius: '290486px'}}
                  className={'footer-icon'}
                  onClick={() => redirectWebsite('https://twitter.com/CalCalvento')}

                    >
                 <TwitterIcon />
                </Button>
                </p>
        </Grid>
        <Grid item md={2} xs={4}>
        <p style={{textAlign: 'center'}}>

        <Button
                  variant="outlined"
                  color="primary"
                  style={{color:'white', border: '2px solid white', borderRadius: '290486px'}}
                  className={'footer-icon'}
                  onClick={() => redirectWebsite('https://gitlab.com/calvincalvento')}

                    >
                 <GitHubIcon />
                </Button>
                </p>
        </Grid>
        <Grid item md={2} xs={4}>
        <p style={{textAlign: 'center'}}>

        <Button
                  variant="outlined"
                  color="primary"
                  style={{color:'white', border: '2px solid white', borderRadius: '290486px'}}
                  className={'footer-icon'}
                  onClick={() => redirectWebsite('https://www.instagram.com/calvs.kleint/')}

                    >
                 <InstagramIcon />
                </Button>
                </p>
        </Grid>
      </Grid>
      <p style={{textAlign: 'center', color:'white', marginTop: '55px'}}>Handcrafted by Calvin Calvento 	&copy; 2020</p>
      </Container>
                    </div>
    </div>
  );
}
