import React, {useState} from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Card from '@material-ui/core/Card';
import Container from '@material-ui/core/Container';
import  "./main.css"
import  "./projects.css"
import designer from './images/designer.svg'
import backend from './images/backend.svg'
import tools from './images/other-tools.svg'
import {Animated} from "react-animated-css";
import Flower from './images/Capture.PNG'
import Rebox from './images/rebox-ecommerce.png'
import ReboxSell from './images/rebox-sell.PNG'
import Akos from './images/akos.png'
import E2e from './images/e2e-system.png'
import ReboxGive from './images/rebox-give.PNG'
import Pdms from './images/online-pdms.PNG'
import Update from './images/update.PNG'
import ReboxLogo from './images/rebox-logo.png'
import FlowerstoreLogo from './images/flowerstore-logo.png'
import Traderzlink from './images/traderzlink.png'
import Button from '@material-ui/core/Button';

export default function Projects() {

   const [project, setProject] = useState(0)



  function redirectWebsite(link){

    window.location.replace(link)
  }

   function onMouse(data){

    setProject(data)
   }

    return(
        <div style={{padding : '5%' , marginTop: '4%'}}>
    <h1>My Recent Projects</h1>
    <p style={{textAlign:'center', fontSize: '20px'}}>Here are the projects I've worked on recently on my previous companies, clients and my personal projects.</p>

    <Grid container  spacing={4} style={{marginTop : '6%'}}>
      <Grid item md={4} xs={12} sm={6}>
        <Card className={'projects website'} onMouseEnter={() =>onMouse(1)} onMouseLeave={() =>onMouse(0)}>
          <div className={'akos-project projects'}>
          {project == 1 ? 
          
          <div style={{padding: '20px'}}><h1>Akosmd</h1> <p style={{textAlign: 'center'}}>-An E-consulting website for patients who wants to consult via online with doctors.
          </p>
           <p style={{
            textAlign: 'center'
          }} >  <Button
          variant="outlined"
          color="primary"
          style={{
            color: "white",
            borderRadius: "290486px",
            border: "2px solid white",
            padding: "10px",
          }}
          className={"website-button"}
          onClick={() => redirectWebsite('https://akosmd.com')}
        >
          VIEW WEBSITE
        </Button></p>
        </div>:
          <img src={Akos} style={{height: '100%', width: '100%', filter: 'brightness(80%)'
          }} /> 
          
         
          }
          </div>
       
          

        </Card>
      </Grid>
      <Grid item md={4} xs={12} sm={6}>
        <Card className={'projects website'} onMouseEnter={() =>onMouse(1)} onMouseLeave={() =>onMouse(0)}>
          <div className={'flower-project projects'}>
          {project == 1 ? 
          
          <div style={{padding: '20px'}}><h1>Flowerstore.ph</h1> <p style={{textAlign: 'center'}}>- An e-commerce website that sells flowers and deliver it to the recepient of the customer.</p>
           <p style={{
            textAlign: 'center'
          }} >  <Button
          variant="outlined"
          color="primary"
          style={{
            color: "white",
            borderRadius: "290486px",
            border: "2px solid white",
            padding: "10px",
          }}
          className={"website-button"}
          onClick={() => redirectWebsite('https://flowerstore.ph')}
        >
          VIEW WEBSITE
        </Button></p>
        </div>:
          <img src={Flower} style={{height: '100%', width: '100%', filter: 'brightness(80%)'
          }} /> 
          
         
          }
          </div>
       
          

        </Card>
      </Grid>
      <Grid item md={4} xs={12} sm={6}>
    
        <Card className={'projects website'} onMouseEnter={() =>onMouse(2)} onMouseLeave={() =>onMouse(0)}>
          <div className={'rebox-project projects'}>
          {project == 2 ?    
          <div style={{padding: '20px'}}><h1>Rebox</h1> <p style={{textAlign: 'center'}}>- An e-commerce website that buys phone to the customers and export it to other countries.</p>
             <p style={{
            textAlign: 'center'
          }} >  <Button
          variant="outlined"
          color="primary"
          style={{
            color: "white",
            borderRadius: "290486px",
            border: "2px solid white",
            padding: "10px",
          }}
          className={"website-button"}
          onClick={() => redirectWebsite('https://rebox.com.sg')}

        >
          VIEW WEBSITE
        </Button></p>
          </div>
          :
          
          <img src={Rebox} style={{height: '100%', width: '100%', filter: 'brightness(80%)'
         }} /> 
          }
          </div>
       
          

        </Card>
      </Grid>
      <Grid item md={4} xs={12} sm={6}>
      <Card className={'projects'} onMouseEnter={() =>onMouse(3)} onMouseLeave={() =>onMouse(0)}>
          <div className={'e2e-project projects'}>
          {project == 3 ?  
          
          <div style={{padding: '20px'}}><h1>E2E System</h1> <p style={{textAlign: 'center'}}>- Content management system of a full flow of flowerstore from the customer ordering from the website to being delivered.</p>
          </div>
          :
          
          <img src={E2e} style={{height: '100%', width: '100%', filter: 'brightness(80%)'
       }} /> 
          }
          </div>
       
          

        </Card>
      </Grid>
      <Grid item md={4} xs={12} sm={6}>
      <Card className={'projects website'} onMouseEnter={() =>onMouse(4)} onMouseLeave={() =>onMouse(0)}>
          <div className={'rebox-sell-project projects'}>
          {project == 4 ?  
          
          <div style={{padding: '20px'}}><h1>Rebox Sell</h1> <p style={{textAlign: 'center'}}>- An e-commerce website that sells phone in Singapore.</p>
            <p style={{
            textAlign: 'center'
          }} >  <Button
          variant="outlined"
          color="primary"
          style={{
            color: "white",
            borderRadius: "290486px",
            border: "2px solid white",
            padding: "10px",
          }}
          className={"website-button"}
          onClick={() => redirectWebsite('http://3.0.20.196/')}

        >
          VIEW WEBSITE
        </Button></p>
          </div>
          :
          
          <img src={ReboxSell} style={{height: '100%', width: '100%', filter: 'brightness(80%)'
       }} /> 
          }
          </div>
       
          

        </Card>
      </Grid>
      <Grid item md={4} xs={12} sm={6}>
      <Card className={'projects'} onMouseEnter={() =>onMouse(5)} onMouseLeave={() =>onMouse(0)}>
          <div className={'rebox-give-project projects'}>
          {project == 5 ?  
          
          <div style={{padding: '20px'}}><h1>Rebox Give</h1> <p style={{textAlign: 'center'}}>- Website for those people who wants to donate their laptop or tablet. </p></div>
          :
          
          <img src={ReboxGive} style={{height: '100%', width: '100%', filter: 'brightness(80%)'
       }} /> 
          }
          </div>
       
          

        </Card>
      </Grid>
      <Grid item md={4} xs={12} sm={6}>
      <Card className={'projects'} onMouseEnter={() =>onMouse(6)} onMouseLeave={() =>onMouse(0)}>
          <div className={'pdms-project projects'}>
          {project == 6 ?  
          
          <div style={{padding: '20px'}}><h1>Online-Pdms</h1> <p style={{textAlign: 'center'}}>- A REST API that connects to other clients platform so that they can pay their loan tickets to the main database.</p></div>
          :
          
          <img src={Pdms} style={{height: '100%', width: '100%', filter: 'brightness(80%)'
       }} /> 
          }
          </div>
       
          

        </Card>
      </Grid>
      <Grid item md={4} xs={12} sm={6}>
      <Card className={'projects website'} onMouseEnter={() =>onMouse(7)} onMouseLeave={() =>onMouse(0)}>
          <div className={'update-project projects'}>
          {project == 7 ?  
          
          <div style={{padding: '20px'}}><h1>Update</h1> <p style={{textAlign: 'center'}}>- A dating website just like tinder with a twist that they can book they person they want to date.</p>
               <p style={{
            textAlign: 'center'
          }} >  <Button
          variant="outlined"
          color="primary"
          style={{
            color: "white",
            borderRadius: "290486px",
            border: "2px solid white",
            padding: "10px",
          }}
          className={"website-button"}
          onClick={() => redirectWebsite('https://tranquil-coast-49208.herokuapp.com/')}

        >
          VIEW WEBSITE
        </Button></p>
          </div>
          :
          
          <img src={Update} style={{height: '100%', width: '100%', filter: 'brightness(80%)'
       }} /> 
          }
          </div>
       
          

        </Card>
      </Grid>
    </Grid>
  
        <div style={{marginTop: '12%'}}>

    <h1 className={'proud'}>I'm proud to worked with these awesome companies:</h1>

    <Grid container  spacing={5}
    direction="row"
    justify="center"
    alignItems="center"
  justify="center"
  alignItems="center" style={{marginTop : '6%'}}>
      <Grid item md={3} xs={12} sm={6}>
        <div className={'worked-companies'}>
        <img src={ReboxLogo} style={{height: '100%', width: '100%'
        }} />
          

        </div>
      </Grid>
      <Grid item md={3} xs={12} sm={6}>
        <div className={'worked-companies'}>
        <img src={FlowerstoreLogo} style={{height: '100%', width: '100%'
        }} />
          

        </div>
      </Grid>
      <Grid item md={3} xs={12} sm={6}>
        <div className={'worked-companies'}>
        <img src={Traderzlink} style={{height: '100%', width: '20%', verticalAlign: 'middle'
        }} /><span style={{fontSize: '23px', marginLeft: '10px'}} className={'traderzlink'}>Traderzlink</span>
          

        </div>
      </Grid>
      </Grid>
        </div>

    
        </div>
       
    )

}
