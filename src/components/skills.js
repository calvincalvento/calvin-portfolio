import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Card from '@material-ui/core/Card';
import Container from '@material-ui/core/Container';
import  "./main.css"
import designer from './images/designer.svg'
import backend from './images/backend.svg'
import tools from './images/other-tools.svg'
import Projects from './projects'
import {Animated} from "react-animated-css";
import {AnimatedOnScroll} from "react-animated-css-onscroll";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    height: 140,
    width: 100,
  },
  control: {
    padding: theme.spacing(2),
  },
}));

export default function SpacingGrid() {
  const [spacing, setSpacing] = React.useState(2);
  const classes = useStyles();

  const handleChange = (event) => {
    setSpacing(Number(event.target.value));
  };

  return (
    <div>
<div>
    <Container maxWidth="lg" style={{marginTop : '-5%' }}>
    <AnimatedOnScroll animationIn="fadeInUp"   isVisible={true}>
        <div className={'skills'}>
    <Grid container className={classes.root} spacing={0}>
      <Grid item md={4} xs={12}>
        <Card className={'cards'}>
          <p style={{textAlign: 'center'}}><img src={backend} /></p>
          <h3 style={{textAlign: 'center', marginTop: '1em'}}>Back-end Developer</h3>
          <p style={{textAlign: 'center'}}>I value simple content structure, clean code patterns,object oriented procedures and thoughtful interactions.</p>
          <p style={{textAlign: 'center', marginTop: '2%', color : '#7510f7', fontWeight: 'bold'}}>Languages, Frameworks And Tools For Back-End Development I Know:</p>
          <p style={{textAlign: 'center', marginTop: '2%'}}>Node.js, Php, Laravel, Nest.js, TypeScript, Vagrant,Rest Api, XAMPP, Express.js, Eloquent, ORM, Postman, JSON </p>
          <p style={{textAlign: 'center', marginTop: '2%', color : '#7510f7', fontWeight: 'bold'}}></p>
          

        </Card>
      </Grid>
      <Grid item md={4} xs={12}>
      <Card className={'cards'}>
          <p style={{textAlign: 'center'}}><img src={designer} /></p>
          <h3 style={{textAlign: 'center', marginTop: '1em', fontWeight: 'bold'}}>Front-end Developer</h3>
          <p style={{textAlign: 'center'}}>I like to code things from scratch, also read other's code and enjoy bringing ideas to life in the browser.</p>
          <p style={{textAlign: 'center', marginTop: '2%', color : '#7510f7', fontWeight: 'bold'}}>Languages, Frameworks And Tools For Front-End Development I Know:</p>
          <p style={{textAlign: 'center', marginTop: '2%'}}>React.js, HTML, CSS, Javascript, Bootstrap, Jquery, AJAX, Redux, Wordpress </p>
          <p style={{textAlign: 'center', marginTop: '2%', color : '#7510f7', fontWeight: 'bold'}}></p>
          <br />

        </Card>
      </Grid>
      <Grid item md={4} xs={12}>
      <Card className={'cards'}>
          <p style={{textAlign: 'center'}}><img src={tools} /></p>
          <h3 style={{textAlign: 'center', marginTop: '1em'}}>Databases & Other Skills</h3>
          <p style={{textAlign: 'center'}}>I usually like to explore more tools and new skills to keep me versatile and efficient as a programmer.</p>
          <p style={{textAlign: 'center', marginTop: '2%', color : '#7510f7', fontWeight: 'bold'}}>Databases & Development Tools I Know:</p><br/>
          <p style={{textAlign: 'center', marginTop: '2%'}}>Mysql, Nosql, Mssql, MongoDb, Linux, Mac, Windows, Git, Trello, Scrum, Db Designer, Heroku,Docker, AWS, Github, Gitlab  </p>
          <p style={{textAlign: 'center', marginTop: '2%', color : '#7510f7', fontWeight: 'bold'}}></p>

        </Card>
      </Grid>
    </Grid>
    </div>
    </ AnimatedOnScroll>
    <Projects />
    </Container>
    </div>
    </div>
    

  );
}
