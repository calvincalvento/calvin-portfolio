import React from 'react';
import logo from './logo.svg';
import Navbar from './components/navbar'
import Banner from './components/banner'
import Skills from './components/skills'
import Projects from './components/projects'
import Testimonials from './components/testimonials'
import Footer from './components/footer'
import './components/main.css'
// import MessengerCustomerChat from 'react-messenger-customer-chat';
import {Helmet} from "react-helmet";
import Logo from './components/images/logo.png';
import script from './script'
import ScriptTag from 'react-script-tag';


function App() {
  return (
  
    <div id="page-container" >
           <Helmet>
                <meta charSet="utf-8" />
                <title>Calvin Calvento</title>
                <link rel="canonical" href="http://mysite.com/example" />
                <link rel="icon" href={Logo}/>

            </Helmet>
      <div id="content-wrap">

      <head>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />
</head>
      <Navbar />
      <Banner />
      <Skills />
      <Testimonials />
      <div style={{marginTop:'150px'}}></div>
      <Footer />
      {/* <MessengerCustomerChat
    pageId="107516994251425"
    appId="533767927548627"
    htmlRef={window.location.pathname}

    />, */}
          <div id="fb-root"></div>

     <ScriptTag isHydrating={true} type="text/javascript" src={script} />

     <div className="fb-customerchat"
      attribution="setup_tool"
      page_id="107516994251425"
logged_in_greeting="Hi! How Can I Help You?"
logged_out_greeting="Hi! How Can I Help You?">
    </div>
    
    </div>
    </div>
  );
}

export default App;
